import React, {Component} from 'react'


class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: new Date(),
      timeMundur : 5,
      showTime : true
    }
  }

    currentTime(){
        this.setState({
            time: new Date()
        })
    }

    componentDidMount(){
        if (this.props.start !== undefined){
        this.setState({timeMundur: this.props.start})
        }
        this.timerID = setInterval(
        () => this.tick(),
        1000
        ); 
    }

    componentDidUpdate()
    {
        if(this.state.timeMundur === 0 && this.state.showTime)
        {
            clearInterval(this.timerID);
            this.hide();
        }
    }

    tick(){
        this.setState({
        timeMundur: this.state.timeMundur - 1 
        });
    }

    hide(){
        this.setState({
            showTime : false
        });
    }

    componentWillUnmount(){
        setInterval(()=> this.currentTime(), 1000);
    }

    render(){
        return(
        <>      
            {
                this.state.showTime &&(
                    <>
                    <h1 style={{float:"left"}}>
                    Sekarang Jam : {this.state.time.toLocaleTimeString("en-US")}
                    </h1>
                    <h1 style={{float:"right"}}>
                    Hitung Mundur : {this.state.timeMundur}
                    </h1>
                    </>
                )
            }  
        </>
        )
    }
}

export default Timer