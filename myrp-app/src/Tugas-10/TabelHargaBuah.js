import React, {Component} from 'react';

class DataBuah extends Component{
    render(){
        return(
            <tr style={{backgroundColor:"#ff7f50"}}>
                <td>{this.props.nama}</td>
                <td>{this.props.harga}</td>
                <td>{this.props.berat/1000} kg</td>
            </tr>
        )
    }   
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class TampilDataBuah extends Component{
    render(){
        return(
            <div style={{padding:"20px"}}>
                <h1 style={{textAlign:"center"}}>Tabel Harga Buah</h1>
                <table border="1px solid #000" style={{width:"100%", margin:"0 auto"}}>
                    <tr style={{textAlign:"center", backgroundColor:"#aaaaaa"}}>
                        <td><b>Nama</b></td>
                        <td><b>Harga</b></td>
                        <td><b>Berat</b></td>
                    </tr>
                    {dataHargaBuah.map(dt=>{
                        return(
                            <>
                            <DataBuah nama={dt.nama} harga={dt.harga} berat={dt.berat}/>
                            </>
                        )
                    })}
                </table>
            </div>
        )
    }    
}

export default TampilDataBuah