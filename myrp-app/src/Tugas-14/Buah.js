import React from "react";
import { BuahProvider } from "./DaftarBuahContext";
import FormBuah from "./FormBuah";
import ListBuah from "./ListBuah";

export default function Buah() {
    return (
        <BuahProvider>
            <ListBuah />
            <FormBuah />
        </BuahProvider>
    );
}
