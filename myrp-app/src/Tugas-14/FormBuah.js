import React, { useContext, useState } from "react";
import { defaultInput, DaftarBuahContext } from "./DaftarBuahContext";
import Axios from "axios";
import "./DaftarBuah.css";

export default function FormBuah() {
    const { buahContext, inputContext } = useContext(DaftarBuahContext);
    const [buah, setBuah] = buahContext;
    const [input, setInput] = inputContext;
    const [isLoading, setIsLoading] = useState(false);

    const handleInputChange = (event) => {
        const id = event.target.id;
        const value = event.target.value;

        setInput({ ...input, ...{ [id]: value } });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);

        // create
        if (input.id === null) {
            Axios.post(
                "http://backendexample.sanbercloud.com/api/fruits",
                input
            )
                .then(function (response) {
                    setBuah(null);
                    setInput(defaultInput);
                    alert(`${response.data.name} berhasil menambahkan data`);
                    setIsLoading(false);
                })
                .catch(function () {
                    alert("Gagal menginput data!");
                    setIsLoading(false);
                });
        } else {
            // update
            Axios.put(
                `http://backendexample.sanbercloud.com/api/fruits/${input.id}`,
                input
            )
                .then(function (response) {
                    setBuah(null);
                    setInput(defaultInput);
                    alert(`${response.data.name} berhasil diubah`);
                    setIsLoading(false);
                })
                .catch(function () {
                    alert("Oops! gagal mengubah data!");
                    setIsLoading(false);
                });
        }
    };

    return (
        <>
        <h1>Form Daftar Harga Buah</h1>

        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
                Nama:
            </label>
            <input style={{float: "right"}} type="text" required id="name" value={input.name} onChange={handleInputChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                Harga:
            </label>
            <input style={{float: "right"}} type="text" required id="price" value={input.price} onChange={handleInputChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                Berat (dalam gram):
            </label>
            <input style={{float: "right"}} type="number" required id="weight" value={input.weight} onChange={handleInputChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
                <button style={{ float: "right"}}>submit</button>
            </div>
            {isLoading && <b style={{ marginLeft: 16 }}>Loading ...</b>}
            </form>
        </div>
        </div>
        </>
    );
}
