import React, { useContext } from "react";
import { DaftarBuahContext } from "./DaftarBuahContext";
import Axios from "axios";
import "./DaftarBuah.css";

export default function ListBuah() {
    const { buahContext, inputContext } = useContext(DaftarBuahContext);
    const [buah, setBuah] = buahContext;
    const [setInput] = inputContext;

    const handleEdit = (event) => {
        const id = event.target.value;
        setInput(buah.find((fruit) => fruit.id === id));
    };

    const handleDelete = (event) => {
        const id = event.target.value;

        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
            .then(function () {
                setBuah(null);
                alert(`Data telah dihapus`);
            })
            .catch(function () {
                alert(`Gagal menghapus data!`);
            });
    };

    return (
        <>
            <h1 style={{ textAlign: "center" }}>Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th style={{ width: 40 }}>No</th>
                        <th style={{ width: "40%" }}>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {buah !== null &&
                        buah.map((item, index) => {
                            return (
                                <tr key={item.id}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{`${item.weight / 1000} kg`}</td>

                                    <td>
                                        <button
                                            value={item.id}
                                            onClick={handleEdit}
                                        >
                                            Edit
                                        </button>
                                        <button
                                            style={{ marginLeft: "1em" }}
                                            value={item.id}
                                            onClick={handleDelete}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </>
    );
}
