import React, { createContext, useState, useEffect } from "react";
import Axios from "axios";

export const DaftarBuahContext = createContext();

export const defaultInput = {
    id: null,
    name: "",
    price: 0,
    weight: 0,
};

export const BuahProvider = (props) => {
    const [buah, setBuah] = useState(null);
    const [input, setInput] = useState(defaultInput);

    useEffect(() => {
        if (buah === null) {
            Axios.get("http://backendexample.sanbercloud.com/api/fruits")
                .then(function (response) {
                    setBuah(response.data);
                })
                .catch(function () {
                    alert("Error!");
                });
        }
    }, [buah]);

    return (
        <DaftarBuahContext.Provider
            value={{
                buahContext: [buah, setBuah],
                inputContext: [input, setInput],
            }}
        >
            {props.children}
        </DaftarBuahContext.Provider>
    );
};
